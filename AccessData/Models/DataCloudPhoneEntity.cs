﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessData.Models
{
    public class DataCloudPhoneEntity
    {
        public string? TotalTimeCall { get; set; }
        public string? RealTimeCall { get; set; }
        public string? LinkFile { get; set; }
    }
}
